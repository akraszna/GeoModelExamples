[![pipeline status](https://gitlab.cern.ch/GeoModelDev/GeoModelExamples/badges/master/pipeline.svg)](https://gitlab.cern.ch/GeoModelDev/GeoModelExamples/commits/master)

# GeoModelExamples

GeoModelExamples contains example programs showing how to use the GeoModel libraries and tools.

Please, refer to the instructions contained in the single subfolders to build specific examples.

----

## Appendix

### Notes on Qt5

To build and run GeoModel I/O libraries, you must have a working Qt5 installation on your computer. Qt5 classes are used for I/O operations with the underlying SQLite daemon, to store/handle data, and to handle logging.

If you are not sure how to install it, please take a look at [the notes on Qt5, in the GeoModelIO repository](https://gitlab.cern.ch/GeoModelDev/GeoModelIO/blob/master/README_QT5_NOTES.md).
